// Context API --> passing information from one component to another without using props.

import React from 'react';

// Stores information
const UserContext = React.createContext();

export const UserProvider = UserContext.Provider;

export default UserContext;
