// [SECTION] DEPENDENCIES FROM REACT
import {useState, useEffect} from 'react';
import {Container} from 'react-bootstrap';
import {BrowserRouter as Router, Route, Routes} from 'react-router-dom';

// [SECTION] FILE IMPORTS
import AppNavBar from './components/AppNavBar';
import Home from './pages/Home';
import Courses from './pages/Courses';
import CourseView from './pages/CourseView';
import Register from './pages/Register';
import Login from './pages/Login';
import Logout from './pages/Logout';
import Error from './pages/Error';


import './App.css';
import {UserProvider} from './UserContext'

// PROVIDER
function App() {

  const [user, setUser] = useState({
    id: null,
    isAdmin: null
  })

  const unsetUser = () => {
    localStorage.clear();
  }

  useEffect(() => {
     
    fetch("http://localhost:4000/users/getUserDetails", {
      headers: {
        Authorization: `Bearer ${localStorage.getItem("token")}`
      }
    })
    .then(res => res.json())
    .then(data => {
      // captured the data of whoever is logged in
      console.log(data)

          // set the user states values with the user details upon successful login
          if(typeof data._id !== "undefined"){
            setUser({
              id: data._id,
              isAdmin: data.isAdmin
            })

          } else {

              // set back the initial state of user
              setUser({
                id: null,
                isAdmin: null
              })
          }
      })

  }, [])

// [SECTION] RENDERING
  return (
    <UserProvider value={{user, setUser, unsetUser}}>
    <Router>
      <AppNavBar/>
      <Container>
      <Routes>
          <Route exact path="/" element={<Home/>}/>
          <Route exact path="/courses" element={<Courses/>}/>
          <Route exact path="/courseView/:courseId" element={<CourseView/>}/>
          <Route exact path="/register" element={<Register/>}/>
          <Route exact path="/login" element={<Login/>}/>
          <Route exact path="/logout" element={<Logout/>}/>
          <Route exact path="*" element={<Error/>}/>
      </Routes>
      </Container> 
    </Router>
    </UserProvider>
  );
};

export default App;
