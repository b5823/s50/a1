import {Row, Col, Card} from 'react-bootstrap';
import {Link} from 'react-router-dom';

export default function CourseCard({courseProp}){

	console.log(courseProp);
	// result: coursesData[0]

	// console.log(typeof props)
	// result: object

	// object destructuring 
	const {name, description, price, _id} = courseProp
	// console.log(name);

	// Syntax: const [getter, setter] = useState(initialValueOfGetter)
	// const [count, setCount] = useState(0)
	// const [seats, setSeat] = useState(30)
	// const [isOpen, setIsOpen] = useState(false)

	// // console.log(useState(0));

	// const enroll = () => {

	// 	if(count === 30){
	// 		alert("No more seats")

	// 	} else {

	// 	setCount(count + 1);
	// 	setSeat(seats - 1);

	// 	}

	// 	console.log(`Enrollees: ${count}`)
	// }

	// useEffect(() => {
	// 	if(seats === 0){
	// 		setIsOpen(true)
	// 	}

	// }, [seats])

	
	return(
		<div>
		<Row>
			<Col>
				<Card className="mb-4">
					<Card.Body>

						<Card.Title>{name}</Card.Title>
						<Card.Subtitle>Description</Card.Subtitle>
						<Card.Text>{description}</Card.Text>
						<Card.Subtitle>Price</Card.Subtitle>
						<Card.Text>{price}</Card.Text>

						<Link className="btn btn-primary" to={`/courseView/${_id}`}>View Details</Link>

						
						{/*Enroll Button*/}
						
						{/*<Card.Text>Enrollees: {count}</Card.Text>
						<Card.Text>Seats Available: {seats}</Card.Text>
						<Button variant="primary" onClick={enroll} disabled={isOpen}>Enroll</Button>*/}


					</Card.Body>
				</Card>
			</Col>
		</Row>
		</div>
	)
}
