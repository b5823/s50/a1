import {useState, useEffect, useContext} from 'react';
import {Form, Button} from 'react-bootstrap';
import {Navigate, Link} from 'react-router-dom';
import Swal from 'sweetalert2'
import UserContext from '../UserContext'

export default function Login(props){
	// console.log(props);

	const {user, setUser} = useContext(UserContext)
	console.log(user);

	// State hooks to store the values of the input fields
	const [mail, setMail] = useState("");
	const [pass, setPass] = useState("");
	// State to deremine whether submit button is enabled or not
	const [isActive, setisActive] = useState(false);

	function loginUser(e){

		e.preventDefault();

		fetch("http://localhost:4000/users/login", {
			method: "POST",
			headers: {
				"Content-Type" : "application/json"
			},
			body: JSON.stringify({
				email: mail,
				password: pass
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)

			if(typeof data.accessToken !== "undefined"){

				localStorage.setItem("token", data.accessToken);
				retrieveUserDetails(data.accessToken);

				Swal.fire({
					title: "Login Sucessful",
					icon: "success",
					text: "Welcome to Booking App of 182!"
				})

			} else {
				Swal.fire({
					title: "Authentication Failed",
					icon: "error",
					text: "Check your credentials"
				})	
			}
		})

		// localStorage.setItem("mail", mail)

		// setUser({
		// 	mail: localStorage.getItem("mail")
		// });

		setMail("");
		setPass("");

		// alert(`${mail} has been verified! Welcome back!`);
	}

	const retrieveUserDetails = (token) => {

		fetch("http://localhost:4000/users/getUserDetails", {
			headers: {
				Authorization: `Bearer ${token}`
			}
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)

			setUser({
				_id: data._id,
				isAdmin: data.isAdmin
			})
		})
	}

useEffect(() => {

	if(mail !== "" && pass !== ""){
		setisActive(true);

	} else{
		setisActive(false);
	} 

}, [mail, pass]);

	return(
		(user.id !== null) ?
		<Navigate to="/courses"/>

		:

		<>
		<h1>Login Here:</h1>
		<Form onSubmit={e => loginUser(e)}>

			<Form.Group controlId="userMail">
		        <Form.Label>Email address</Form.Label>
		        <Form.Control 
		        	type="email" 
		        	placeholder="Enter mail"
		        	required
		        	value={mail}
		        	onChange={e => setMail(e.target.value)} 
		        />
      		</Form.Group>

      		<Form.Group controlId="pass">
		        <Form.Label>Password</Form.Label>
		        <Form.Control 
		        	type="password" 
		        	placeholder="Password"
		        	required
		        	value={pass}
		        	onChange={e => setPass(e.target.value)}
		        />
      		</Form.Group>

      		<p>Not yet registered? <Link to="/register">Register here!</Link></p>

      		{ isActive ?
      		<Button variant="success" type="submit" id="loginBtn" className="mt-3 mb-5">
      			Submit
      		</Button>

      		  :

      		<Button variant="danger" type="submit" id="loginBtn" className="mt-3 mb-5" disabled>
      			Submit
      		</Button>
      		} 

		</Form>
		</>
	)
};
