import {useContext, useEffect} from 'react';
import {Navigate} from 'react-router-dom';
import UserContext from "../UserContext";

export default function Logout(){

	const {setUser, unsetUser} = useContext(UserContext);

	// clearing the local storage information
	unsetUser();

	useEffect(() => {

		setUser({
			id: null,
			isAdmin: null
		})

	}, [])

	return(

		<Navigate to="/login"/>
	)
};
