import {Row, Col} from 'react-bootstrap';

import {Link} from 'react-router-dom';

export default function Error(){
	return(
		<Row>
			<Col className = "p-5">
				<h1>404 Page Not Found</h1>
				<p className="mb-4">The page you were looking for does not exist.</p>
				<p>Go back to the <Link to="/">homepage</Link>.</p>
				
			</Col>
		</Row>
	)
}